// Copyright (c) 2020 Valentin Kahl
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

#![windows_subsystem = "windows"]

use msgbox::{create as msgbox, IconType};
use std::{
    fs,
    io::{prelude::*, BufReader, BufWriter, SeekFrom},
    path::PathBuf,
    process::Command,
    time::SystemTime,
};
use structopt::StructOpt;

type DynResult<T> = Result<T, Box<dyn std::error::Error>>;

const MAX_SEEK_LEN: usize = 512;

/// The structure of command line arguments
#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(short, long, required = true)]
    file:    PathBuf,
    #[structopt(short, long, required = true)]
    start:   usize,
    #[structopt(short, long, required = true)]
    end:     usize,
    #[structopt(short = "x", long, required = true)]
    execute: String,
}

/// An extension trait for convenient parsing
trait WriteExt: Write {
    fn from_reader_until(
        &mut self,
        reader: &mut impl Read,
        pat: &[u8],
        buf: &mut [u8],
        max_bytes: usize,
    ) -> std::io::Result<usize> {
        let mut bytes_copied = 0;
        while &buf[0..pat.len()] != pat && bytes_copied < max_bytes {
            reader.read_exact(buf)?;
            self.write_all(buf)?;
            bytes_copied += buf.len();
        }
        Ok(bytes_copied)
    }

    fn from_reader(
        &mut self,
        reader: &mut impl Read,
        buf: &mut [u8],
        max_bytes: usize,
    ) -> std::io::Result<usize> {
        let mut bytes_copied = 0;
        while bytes_copied < max_bytes {
            match reader.read(buf) {
                Ok(bytes_read) if bytes_read > 0 => {
                    self.write_all(&buf[0..bytes_read])?;
                    bytes_copied += bytes_read;
                },
                Err(e) => return Err(e),
                _ => break,
            };
        }
        Ok(bytes_copied)
    }
}

impl<T: Write> WriteExt for T {}

/// Create a uniqe temporary file path in the same dir as the input file
fn temp_file_path(opt: &Opt) -> DynResult<PathBuf> {
    Ok(opt.file.with_file_name(&format!("_sb_tmp_{}.wav", time_micros()?)))
}
fn time_micros() -> DynResult<u128> {
    Ok(SystemTime::now().duration_since(SystemTime::UNIX_EPOCH)?.as_micros())
}

/// Entry point
/// calls [`main_fallible`] and opens a message box if it fails
///
/// [`main_fallible`]: functions.main_fallible.html
fn main() {
    if let Err(e) = main_fallible() {
        msgbox("seq-bridge error", &e.to_string(), IconType::Error);
        std::process::exit(1);
    }
}

/// The actual main function
fn main_fallible() -> DynResult<()> {
    let opt = Opt::from_iter_safe(std::env::args())?;
    let tmp_path = temp_file_path(&opt)?;

    let result = (|| {
        let (offset, length) = create_and_fill_tempfile(&opt, &tmp_path)?;
        execute_command(&opt, &tmp_path)?;
        apply_changes_to_original(&opt, &tmp_path, offset, length)?;
        Ok(fs::remove_file(&tmp_path)?)
    })();

    // make another attempt to clean up the temp. file in case one of the previous
    // steps failed
    fs::remove_file(&tmp_path).ok();

    result
}

fn create_and_fill_tempfile(opt: &Opt, tmp_path: &PathBuf) -> DynResult<(usize, usize)> {
    let mut tmp = BufWriter::new(fs::File::create(tmp_path)?);
    let mut infile = BufReader::new(fs::File::open(&opt.file)?);

    let mut header_len = 0;
    let mut buf_2 = [0u8; 2];
    let mut buf_4 = [0u8; 4];

    header_len += match tmp.from_reader_until(&mut infile, b"RIFF", &mut buf_4, MAX_SEEK_LEN) {
        Ok(bytes_copied) if bytes_copied < MAX_SEEK_LEN => bytes_copied,
        Err(e) => return Err(e.into()),
        _ => return Err("Invalid wav file (no 'RIFF' signature)".into()),
    };

    header_len += match tmp.from_reader_until(&mut infile, b"WAVE", &mut buf_4, MAX_SEEK_LEN) {
        Ok(bytes_copied) if bytes_copied < MAX_SEEK_LEN => bytes_copied,
        Err(e) => return Err(e.into()),
        _ => return Err("Invalid wav file (no 'WAVE' signature)".into()),
    };

    header_len += match tmp.from_reader_until(&mut infile, b"fmt ", &mut buf_4, MAX_SEEK_LEN) {
        Ok(bytes_copied) if bytes_copied < MAX_SEEK_LEN => bytes_copied,
        Err(e) => return Err(e.into()),
        _ => return Err("Invalid wav file (no 'fmt' chunk)".into()),
    };

    header_len += tmp.from_reader(&mut infile, &mut buf_4, 4)?; // ignore fmt chunk size

    infile.read_exact(&mut buf_2)?;
    let audio_format = u16::from_le_bytes(buf_2);
    // supported audio formats: 1 (WAVE_FORMAT_PCM), 3 (WAVE_FORMAT_IEEE_FLOAT)
    if audio_format != 1 && audio_format != 3 {
        return Err(format!("Unsupported audio format (not PCM): 0x{:02x}", audio_format).into());
    }
    tmp.write_all(&buf_2)?;
    header_len += 2;

    infile.read_exact(&mut buf_2)?;
    header_len += 2;
    let channels = u16::from_le_bytes(buf_2) as usize;
    if channels > 8 {
        return Err(format!("To many channels ({})", channels).into());
    }
    tmp.write_all(&buf_2)?;

    header_len += tmp.from_reader(&mut infile, &mut [0u8; 10], 10)?;

    infile.read_exact(&mut buf_2)?;
    header_len += 2;
    let bit_depth = u16::from_le_bytes(buf_2);
    let bytes_per_sample = (bit_depth / 8) as usize;
    if !&[2, 3, 4, 8].contains(&bytes_per_sample) {
        return Err(format!("Invalid bit depth ({})", bit_depth).into());
    }
    tmp.write_all(&buf_2)?;

    header_len += match tmp.from_reader_until(&mut infile, b"data", &mut buf_4, MAX_SEEK_LEN) {
        Ok(bytes_copied) if bytes_copied < MAX_SEEK_LEN => bytes_copied,
        Err(e) => return Err(e.into()),
        _ => return Err("Not a proper wav file (no data signature)".into()),
    };

    infile.read_exact(&mut buf_4)?;
    header_len += 4;
    let data_block_len = u32::from_le_bytes(buf_4) as usize;

    let start_i = header_len + channels * bytes_per_sample * opt.start;
    let end_i = header_len + channels * bytes_per_sample * opt.end;

    let data_len = match end_i.checked_sub(start_i) {
        Some(v) => v,
        None => return Err("Sample values out of range (start, end or length to small)".into()),
    };

    if start_i - header_len > data_block_len || end_i - header_len > data_block_len {
        return Err("Sample values out of range (start or end to big)".into());
    }

    tmp.write_all(&(data_len as u32).to_le_bytes())?;
    infile.seek(SeekFrom::Start(start_i as u64))?;
    tmp.from_reader(&mut infile, &mut [0u8; 128], data_len)?;

    // correct chunk size
    tmp.seek(SeekFrom::Start(4))?;
    tmp.write_all(&((header_len + data_len - 8) as u32).to_le_bytes())?;

    tmp.flush()?;

    Ok((start_i, data_len))
}

fn execute_command(opt: &Opt, tmp_path: &PathBuf) -> DynResult<()> {
    Command::new(&opt.execute).arg(tmp_path).status()?;
    Ok(())
}

fn apply_changes_to_original(
    opt: &Opt,
    tmp_path: &PathBuf,
    offset: usize,
    length: usize,
) -> DynResult<()> {
    let mut tmp = BufReader::new(fs::File::open(tmp_path)?);
    let mut infile = BufWriter::new(fs::OpenOptions::new().write(true).open(&opt.file)?);

    // seek until data signature
    let mut buf = [0u8; 4];
    let mut seeked = 0;
    while &buf != b"data" {
        tmp.read_exact(&mut buf)?;
        seeked += buf.len();
        if seeked > MAX_SEEK_LEN {
            return Err("The temporary file has been corrupted (can't find data chunk).".into());
        }
    }
    // ignore chunk size
    tmp.seek(SeekFrom::Current(4))?;

    infile.seek(SeekFrom::Start(offset as u64))?;
    infile.from_reader(&mut tmp, &mut [0u8; 128], length)?;
    infile.flush()?;
    Ok(())
}
